"""
Python frontend for controlling Network POE switch using SNMP

"""

import midas
import midas.frontend
import midas.event
import collections
import ctypes
import os
import subprocess
from time import time
import shlex




class MyPeriodicEquipment(midas.frontend.EquipmentBase):

    def __init__(self, client):
        # The name of our equipment. This name will be used on the midas status
        # page, and our info will appear in /Equipment/MyPeriodicEquipment in
        # the ODB.
        equip_name = "PoeSwitch"
        
        # Define the "common" settings of a frontend. These will appear in
        # /Equipment/MyPeriodicEquipment/Common. The values you set here are
        # only used the very first time this frontend/equipment runs; after 
        # that the ODB settings are used.
        default_common = midas.frontend.InitialEquipmentCommon()
        default_common.equip_type = midas.EQ_PERIODIC
        default_common.buffer_name = "SYSTEM"
        default_common.trigger_mask = 0
        default_common.event_id = 1
        default_common.period_ms = 1000
        default_common.read_when = midas.RO_ALWAYS
        default_common.log_history = 1

        self.numPorts = 8

        # Create the settings directory.  
        IntType = ctypes.c_int32 * self.numPorts
        BoolType = ctypes.c_bool * self.numPorts
        StringType = ctypes.c_char_p * self.numPorts 
        POEE_Names = []
        POEO_Names = []        
        POES_Names = []        
        Port_Description = []        
        for i in range(self.numPorts):
            POEE_Names.append("POE Enable Port " + str(i+1))
            POEO_Names.append("Port link status " + str(i+1))
            POES_Names.append("Port link speed " + str(i+1))
            Port_Description.append("Port Description " + str(i+1))
        POEB_Names = ["Switch POE power usage"]

        default_settings = collections.OrderedDict([  
            ("host_ip", ctypes.create_string_buffer(bytes("192.168.0.251", "utf-8"), 32)),
            ("community", ctypes.create_string_buffer(bytes("public", "utf-8"), 32)),
            ("port_enable", BoolType(*[1] * self.numPorts)),
            ("Names POEE", POEE_Names),
            ("Names POEO", POEO_Names),
            ("Names POES", POES_Names),
            ("Names POEB", POEB_Names),
            ("Port Description",Port_Description)
        ])                                                                             
        
        # You MUST call midas.frontend.EquipmentBase.__init__ in your equipment's __init__ method!
        midas.frontend.EquipmentBase.__init__(self, client, equip_name, default_common, default_settings)

        # Create some fields for readback strings
        self.odb_readback_dir = self.odb_settings_dir.replace("Settings", "Readback")
        if not self.client.odb_exists(self.odb_readback_dir):
            PortStatus = []
            for i in range(self.numPorts):
                PortStatus.append("Detection Status Port " + str(i+1))

            readback = collections.OrderedDict([
                ("Port Status", PortStatus)
            ])
            
            self.client.odb_set(self.odb_readback_dir, readback) 


        # Get info for SNMP
        self.host_ip = self.client.odb_get(self.odb_settings_dir + "/host_ip")
        self.community = self.client.odb_get(self.odb_settings_dir + "/community")

        self.client.msg("Using IP = " + self.host_ip + " and community=" + self.community + " for SNMP.")

        # Setup callback on port enable ODB keys
        self.client.odb_watch(self.odb_settings_dir + "/port_enable", self.port_enable_callback)

        # cache the current value of the port_enable key
        self.cache_port_enable = self.client.odb_get(self.odb_settings_dir + "/port_enable")


        # You can set the status of the equipment (appears in the midas status page)
        self.set_status("Initialized")

    def snmpget(self, host, community, oid):
        """
        Return snmpget command and return result
        """

        command = "snmpget -v 2c -c " + community + " " + host + " " + oid + "  -m ./POWER-ETHERNET-MIB.txt -O vUq"
        #print(command)
        process = subprocess.run(shlex.split(command),
                                 stdout=subprocess.PIPE,
                                 universal_newlines=True)

        return process.stdout.strip('\n')

    def snmpset(self, host, community,oid, value):
        """
        Execute snmpset command and return result (only works for int)
        """

        command = "snmpset -v 2c -c " + community + "  -m ./POWER-ETHERNET-MIB.txt" + " " + host + " " + oid + " i " + str(value)
        print(command)                                                                                                                                                                             

        process = subprocess.run(shlex.split(command),
                                 stdout=subprocess.PIPE,
                                 universal_newlines=True)
        print(process.stdout)

        #return process.stdout.strip('\n')

        
        
    def readout_func(self):
        
        # In this example, we just make a simple event with one bank.
        event = midas.event.Event()

        # Bank for POE power enable
        data = []
        for i in range(self.numPorts):
            rvalue = self.snmpget(self.host_ip,self.community,"POWER-ETHERNET-MIB::pethPsePortAdminEnable.1."+str(i+1))
            if rvalue == 'true':
                data.append(True)
            else:
                data.append(False)
            
        print(data)
        event.create_bank("POEE", midas.TID_BOOL, data)

        # Bank for POE Port operational status
        data1 = []
        for i in range(self.numPorts):
            rvalue = self.snmpget(self.host_ip,self.community,"RFC1213-MIB::ifOperStatus."+str(i+1))
            if rvalue == "up":
                data1.append(True)
            else:
                data1.append(False)
        print(data1)
        event.create_bank("POEO", midas.TID_BOOL, data1)

        # Bank for POE Port speed
        data2 = []
        for i in range(self.numPorts):
            rvalue = self.snmpget(self.host_ip,self.community,"IF-MIB::ifHighSpeed."+str(i+1))
            data2.append(int(rvalue))
        print(data2)
        event.create_bank("POES", midas.TID_INT, data2)

        # Bank for general board status
        data3 = []
        power = float(int(self.snmpget(self.host_ip,self.community,"POWER-ETHERNET-MIB::pethMainPseConsumptionPower.1")))
        powerf = power / 1000.0

        data3.append(powerf)
        event.create_bank("POEB", midas.TID_FLOAT, data3)


        # Now fill some general status strings that don't go nicely into banks
        for i in range(self.numPorts):
            rvalue = self.snmpget(self.host_ip,self.community,"POWER-ETHERNET-MIB::pethPsePortDetectionStatus.1."+str(i+1))
            self.client.odb_set(self.odb_readback_dir+"/Port Status["+str(i)+"]",rvalue)


        return event


    def port_enable_callback(self,client, path, odb_value):
        """
        Callback function when port enable key is changed
        """
        print("New ODB content at %s is %s" % (path, odb_value))
        
        # Make sure we are dealing with the right key
        if "port_enable" in path:
            
            # Check all the ODB keys, see if any changed
            for i in range(len(odb_value)):
                if odb_value[i] != self.cache_port_enable[i]:
                    print( "The Port enable for chan " + str(i) + " changed to " + str(odb_value[i]))
                    if odb_value[i]:
                        # Turn on port
                        self.client.msg("Enabling POE for ethernet channel " + str(i+1))
                        self.snmpset(self.host_ip,self.community,"POWER-ETHERNET-MIB::pethPsePortAdminEnable.1."+str(i+1), 1)
                    else:
                        # Turn off port
                        self.client.msg("Disabling POE for ethernet channel " + str(i+1))
                        self.snmpset(self.host_ip,self.community,"POWER-ETHERNET-MIB::pethPsePortAdminEnable.1."+str(i+1), 2)

        # cache the current status of switch ports
        self.cache_port_enable = odb_value
        




class MyFrontend(midas.frontend.FrontendBase):
    """
    """
    def __init__(self):
        # You must call __init__ from the base class.
        midas.frontend.FrontendBase.__init__(self, "fepoeswitch")
        
        # You can add equipment at any time before you call `run()`, but doing
        # it in __init__() seems logical.
        self.add_equipment(MyPeriodicEquipment(self.client))

    def begin_of_run(self, run_number):
        self.set_all_equipment_status("Running", "greenLight")
        self.client.msg("Frontend has seen start of run number %d" % run_number)
        return midas.status_codes["SUCCESS"]
        
    def end_of_run(self, run_number):
        self.set_all_equipment_status("Finished", "greenLight")
        self.client.msg("Frontend has seen end of run number %d" % run_number)
        return midas.status_codes["SUCCESS"]
        
if __name__ == "__main__":


    # The main executable is very simple - just create the frontend object,
    # and call run() on it.
    my_fe = MyFrontend()
    my_fe.run()
