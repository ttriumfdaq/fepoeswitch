
# Setup the python environment.  Needs to be at least python 3.0 I think.

export VIRTUAL_ENV_DISABLE_PROMPT=1
source ~/python3_env/bin/activate

cd /home/mpmtdaq/online/fepoeswitch

python fePoeSwitch.py
